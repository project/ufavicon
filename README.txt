README.txt for Zomato module
---------------------------

CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration

INTRODUCTION
------------

This module allow Users to have their own favicon same as there User Picture.
After User gets login into website, user will be able to see their
own favicon on the browser.


REQUIREMENTS
------------

This module requires no modules outside of Drupal core.

RECOMMENDED MODULES
-------------------

Markdown filter (https://www.drupal.org/project/markdown):
When enabled, display of the project's README.md help will be rendered
with markdown.

INSTALLATION
------------

 - Install the uFavicon module as you would normally install
   a contributed Drupal module.
   Visit https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
------------

* This module add one field at User entity i.e User Picture Favicon

  - Go to admin/config/people/accounts/form-display

    On this page enabled this field on User Profile form by
    dragging "User Picture Favicon" field from Disabled field section.
